import os
import tempfile
import boto3


class _Upload:
    def __init__(self) -> None:
        pass

    def _run(self, bucket, file: str, s3_path: str):
        self.file = file
        self.s3_path = s3_path
        self.client = boto3.resource("s3")
        self.bucket = self.client.Bucket(name=bucket)

        self._create_file()

    def _create_file(self):
        with tempfile.TemporaryDirectory() as tmp:
            filepath = os.path.join(
                tmp,
                "index.html",
            )
            file = open(filepath, "w")
            with file as f:
                f.write(self.file)

            self._upload(filepath)

    def _upload(self, index):
        key = os.path.join(self.s3_path, "index.html")
        # print(key)
        self.bucket.upload_file(
            Filename=index, Key=key, ExtraArgs={"ContentType": "text/html"}
        )


_uploader = _Upload()


def upload(bucket, file, s3_path):
    return _uploader._run(bucket, file, s3_path)


if __name__ == "__main__":
    upload(
        "pip.trainland.io", "/home/mantix/prog/python/s3-index-gen/test.txt", "fake/"
    )
