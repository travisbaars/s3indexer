import argparse

from .build import build
from .parser_formatter import CustomHelpFormatter


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="s3indexer", formatter_class=CustomHelpFormatter
    )
    subparsers = parser.add_subparsers(metavar="command", dest="command")

    # create the run sub-command
    subparser_run = subparsers.add_parser("run", help="run the whole shebang")
    subparser_run.add_argument(
        "bucket", help="name of bucket in which to create index files"
    )
    subparser_run.add_argument(
        "--local",
        action="store_true",
        default=False,
        help="generate index files locally",
    )
    subparser_run.add_argument(
        "--dry-run",
        action="store_true",
        default=False,
        dest="dryrun",
        help="print bucket instead of running build",
    )

    parser.add_argument

    args = parser.parse_args()

    match args.command:
        # sub-command [run]
        case "run":
            ### add a confirmation before running buld()
            if not type(args.bucket) == str:
                raise TypeError("bucket must be a string")

            if "http://" in args.bucket or "https://" in args.bucket:
                raise ValueError("name of bucket expected, got bucket url")

            if args.dryrun:
                print("--DRY-RUN", "|", args.bucket)

            # if args.local:
            #     build(args.bucket)

            build(args.bucket)

            ### add [run --version] argument which creates an index for a single version instead
            ### of rewriting all the indexes. should save on bucket read/writes.
            # if --version:
            ##  require --title --header --links
            #   match "data":
            #     case not "title":
            #       raise "title needed when using --version"
            #     ...

            #   generate(args.title, args.header, args.links)

        ### add new subcommand: [gather]                                     >
        ### takes (path/to/pyproject.toml) as argument                       > maybe different module??
        ### gets package name and version from toml file for use in pipeline >
