class _HtmlGenerator:
    """
    This class is used to programatically create simple html files
    """

    def __init__(self) -> None:
        pass

    def _run(self, title: str, header: str, links: list):
        """
        starting method
        """
        self.title = f"""<title>{title}</title>"""
        self.header = f"""<h1>{header}</h1>"""
        self.links = links

        self.index = self._generate_index()

        return self.index

    def _generate_index(self):
        newline = "\n    "
        link_gen = f"""{newline.join(f'<a href="{value}">{value}</a><br />' for value in self.links)}"""

        scaffold = f"""
<html>
  <head>
    {self.title}
  </head>
  <body>
    {self.header}
    <br />
    {link_gen}
  </body>
</html>
"""

        return scaffold


_generator = _HtmlGenerator()


def generate(title: str, header: str, links: list):
    """
    this is the public method for the _HtmlGenerator class
    """
    return _generator._run(title, header, links)
