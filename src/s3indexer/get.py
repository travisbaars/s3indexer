import boto3


class _GetBucketLayout:
    def __init__(self) -> None:
        # set boto3 to use the s3 resource
        self.client = boto3.resource("s3")

    def _start(self, bucket):
        # setup s3 bucket object
        self.bucket = self.client.Bucket(name=bucket)

        # get packages from s3 bucket and turn into a dictionary
        self.container = self._recurse_bucket()

        return self.container

    def _recurse_bucket(self) -> dict:
        container = {}

        for obj in self.bucket.objects.all():
            # simple/0.0.1/somefile.whl
            key = obj.key

            # ignores any object that contains an index.html
            if not "index.html" in str(obj):
                # creates 3 parts to each path (project, version, files)
                # project, version, file = key.split("/")
                project, file = key.split("/")

                # if not project in container:
                #     container[project] = {"versions": [version], "files": [file]}
                # else:
                #     if version not in container[project]["versions"]:
                #         container[project]["versions"].append(version)

                #     if file not in container[project]["files"]:
                #         container[project]["files"].append(file)

                if not project in container:
                    container[project] = {"files": [file]}
                else:
                    if file not in container[project]["files"]:
                        container[project]["files"].append(file)

        return container


_collecter = _GetBucketLayout()


def get_bucket_layout(bucket):
    return _collecter._start(bucket)


if __name__ == "__main__":
    container = get_bucket_layout("pip.trainland.io")
    for key, value in container.items():
        print(f"{key}: {value}")
